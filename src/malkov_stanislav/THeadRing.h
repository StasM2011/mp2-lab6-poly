#pragma once

#include "TDatList.h"

class THeadRing : public TDatList {
protected:
    PTDatLink pHead;
public:
    THeadRing() {
        pHead = new TDatLink();
    }

    ~THeadRing() {
        // ����������, ���������� � TDatList, ��������������� ������� �������� 
        // ���� �� �������� pStop, � � THeadRing �� pLast ������� pHead
        if(pLast != nullptr)
            pLast->SetNextLink(pStop);
        delete pHead;
    }

    virtual void InsFirst(PTDatValue pVal = nullptr) {
        TDatList::InsFirst(pVal);
        pHead->SetNextLink(pFirst);
        pLast->SetNextLink(pHead);
    }

    virtual void InsLast(PTDatValue pVal = nullptr) {
        if (pFirst == nullptr)
            InsFirst(pVal);
        else {
            TDatList::InsLast(pVal);
            pLast->SetNextLink(pHead);
        }
    }

    virtual void InsCurrent(PTDatValue pVal = nullptr) {
        if (pFirst == nullptr)
            InsFirst(pVal);
        else
            TDatList::InsCurrent(pVal);
    }

    virtual void DelFirst(void) {
        TDatList::DelFirst();
        pHead->SetNextLink(pFirst);
        if (pFirst == pHead)
            pFirst = nullptr;
    }

    // �������������� �����, ��������� ����� pStop �� ������������, � �������� �� TDatList
    virtual void DelList(void) {
        if (pFirst == nullptr)
            return;
        PTDatLink link = pFirst;
        while (link->GetNextDatLink() != pHead) {
            PTDatLink delLink = link;
            link = link->GetNextDatLink();
            delete delLink;
        }
        pFirst = nullptr;
        pLast = nullptr;
        pCurrLink = nullptr;
        pPrevLink = nullptr;
        ListLen = 0;
    }

    virtual int IsListEnded(void) const {
        return pCurrLink == pHead || pFirst == nullptr;
    }
};
