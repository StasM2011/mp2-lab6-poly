#include <iostream>
#include <gtest\gtest.h>
#include "TPolinom.h"



int main(int argc, char** argv) {
    // Google Tests
    ::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    // Example
    setlocale(LC_CTYPE, "Russian");
    TPolinom* p = new TPolinom();
    p->InsFirst(new TMonom(1, 2));  // z^2
    p->InsFirst(new TMonom(2, 10)); // 2y
    std::cout << "��������� ���������� z^2 + 2*y � ����� (0, 1, 2): " << p->Calculate(0, 1, 4);  // (2y + z^2) <- (0, 1, 2) == 2 * 1 + 2^2 = 6
    getchar();
    return 0;
}