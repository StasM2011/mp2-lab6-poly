#include "TPolinom.h"

#include <gtest/gtest.h>

TEST(TPolinom, CanGetMonom) {
    TPolinom polinom;
    polinom.InsFirst(new TMonom(2, 2));
    PTMonom monom = polinom.GetMonom();
    EXPECT_EQ(monom->GetCoeff(), 2);
    EXPECT_EQ(monom->GetIndex(), 2);
}

TEST(TPolinom, CanInitializePolinom) {
    int p[1][2] = { {1, 2} };
    TPolinom polinom(p, 1);
    EXPECT_EQ(polinom.GetMonom()->GetCoeff(), 1);
    EXPECT_EQ(polinom.GetMonom()->GetIndex(), 2);
}

TEST(TPolinom, CanCopyPolinom) {
    TPolinom p1;
    p1.InsFirst(new TMonom(1, 1));
    TPolinom p2(p1);
    EXPECT_EQ(p2.GetMonom()->GetCoeff(), 1);
    EXPECT_EQ(p2.GetMonom()->GetIndex(), 1);
}

TEST(TPolinom, CanAddPolinom) {
    TPolinom p1;
    TPolinom p2;
    p2.InsFirst(new TMonom(1, 1));
    p1 = p1 + p2;
    EXPECT_EQ(p1.GetListLength(), 1);
}

TEST(TPolinom, CanRemovePolinom) {
    TPolinom p1;
    p1.InsFirst(new TMonom(1, 2));
    TPolinom p2(p1);
    p1 = p2 - p1;
    EXPECT_EQ(p1.GetListLength(), 0);
}

TEST(TPolinom, CanAssignPolinom) {
    TPolinom p1, p2;
    p1.InsFirst(new TMonom(1, 2));
    p2 = p1;
    EXPECT_EQ(p2.GetMonom()->GetIndex(), 2);
}

TEST(TPolinom, CanAssignPolinomToSelf) {
    TPolinom p1;
    p1.InsFirst(new TMonom(1, 2));
    p1 = p1;
    EXPECT_EQ(p1.GetMonom()->GetIndex(), 2);
}

TEST(TPolinom, CanMultiplyPolinom) {
    int p[2][2] = { {1, 100}, {1, 10} }; // x + y
    TPolinom p1(p, 2);
    TPolinom p2(p, 2);
    p1 = p1 * p2;   // (x + y) * (x + y) = x^2 + 2xy + y^2
    EXPECT_EQ(p1.GetListLength(), 3);
}

TEST(TPolinom, CanCalculate) {
    int p[2][2] = { {1, 100}, {1, 10} };
    TPolinom p1(p, 2);
    EXPECT_EQ(p1.Calculate(10, 30), 40);    // x + y : (10, 30) = 40
}